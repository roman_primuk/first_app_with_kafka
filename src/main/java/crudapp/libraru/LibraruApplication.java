package crudapp.libraru;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.Closeable;
import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@SpringBootApplication
public class LibraruApplication {

    public static void main(String[] args) {
//        SpringApplication.run(LibraruApplication.class, args);
        String topic = "spring-kafka-demo";
        var producer = new MyProducer(topic);
        Runnable r = () -> {
            for (int i = 0; i <= 100; i++) {
                producer.send(String.valueOf(i), "Hello from MyProducer");
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(r).start();
        var consumer = new MyConsumer(topic);
        consumer.consume(new Consumer<ConsumerRecord<String, String>>() {
            @Override
            public void accept(ConsumerRecord<String, String> stringStringConsumerRecord) {
                System.out.println("Got key: " + stringStringConsumerRecord.key() + ", value: " + stringStringConsumerRecord.value());
            }
        });
        try {
            TimeUnit.MINUTES.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        producer.send("key", "value");
//        try {
//            producer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}

class MyConsumer implements Closeable {

    private String topic;

    private KafkaConsumer<String, String> consumer = getConsumer();

    public MyConsumer(String topic) {
        this.topic = topic;
    }

    private KafkaConsumer<String, String> getConsumer() {
        var props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "clientId");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        var consumer = new KafkaConsumer<String, String>(props);
        consumer.subscribe(Collections.singleton(topic));
        return consumer;
    }

    void consume(Consumer<ConsumerRecord<String, String>> recordsConsumer) {
        Runnable r = () -> {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));
                records.forEach(recordsConsumer::accept);
            }
        };
        new Thread(r).start();
    }

    @Override
    public void close() throws IOException {
        consumer.close();
    }
}

class MyProducer implements Closeable {

    private String topic;

    private KafkaProducer<String, String> producer = getProducer();

    public MyProducer(String topic) {
        this.topic = topic;
    }

    private KafkaProducer<String, String> getProducer() {
        var props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "clientId");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new KafkaProducer<>(props);
    }

    public void send(String key, String value) {
        try {
            producer
                    .send(new ProducerRecord(topic, key, value))
                    .get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            producer.close();
        }
    }

    @Override
    public void close() throws IOException {
        producer.close();
    }
}